## Get the source
Get the source code by cloning this repository by locating the link to the repository, the link is available in the top right corner in the "Overview" tab.

```
git clone <repository>
```

## Install the dependencies
Install the dependencies for the frontend part of the project by running the following commands from the root of the whole project.

First go to the frontend folder (all of the npm commands needs to be executed inside the frontend folder for the intended results to be true as described below).

```
cd frontend/
```

Then run the installation command for npm, this will retrieve the dependencies located in the package.json configuration file.

```
npm install
```

## Start the local server

To start the local webserver run the start command for npm, make sure you are still located in the frontend folder.
```
npm start
```

This will start a local webserver on the following adress. Open up a browser of your choice and enter the following:
```
localhost:3000
```

## Running the tests available for the project
Make sure that you've gone through the installation process described above.

Run the unit tests
```
npm test
```

Run the unit & e2e (CI) tests
```
npm run ci
```

## Misc commands

The build command that creates a distributed/static version of the current source code, for development/staging use.
```
npm run build & npm run build:dev
```

The above will use the development configuration located in
```
config/webpack.dev.js
```

The build command that creates a distribution/static version of the current source code, for production use.
```
npm run build:prod
```

The above will use the production configuration located in. And will fail as soon as a error is encountered as to ensure that no failing builds will go to the production environment.
```
config/webpack.prod.js
```

The rest of the available commands are located in the package.json configuration file. Most of them are commands used by other commands so they are almost never used by (you) the users.







