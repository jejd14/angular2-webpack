#!/bin/bash
# Script is tested on Ubuntu 16.04
# Script needs to be executed with . ./installMySQL.bash

export DEBIAN_FRONTEND="noninteractive"

mySqlRootPassword="password"

echo "mysql-server-5.7 mysql-server/root_password password $mySqlRootPassword" | sudo debconf-set-selections
echo "mysql-server-5.7 mysql-server/root_password_again password $mySqlRootPassword" | sudo debconf-set-selections

# Install MySQL 5.7
sudo apt-get install -y mysql-server-5.7

# Install Expect
sudo apt-get install -y expect

# Build Expect script
tee ~/secure_our_mysql.sh > /dev/null << EOF
spawn $(which mysql_secure_installation)

expect "Enter password for user root:"
send "$mySqlRootPassword\r"

expect "Press y|Y for Yes, any other key for No:"
send "y\r"

expect "Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG:"
send "2\r"

expect "Change the password for root ? ((Press y|Y for Yes, any other key for No) :"
send "n\r"

expect "Remove anonymous users? (Press y|Y for Yes, any other key for No) :"
send "y\r"

expect "Disallow root login remotely? (Press y|Y for Yes, any other key for No) :"
send "y\r"

expect "Remove test database and access to it? (Press y|Y for Yes, any other key for No) :"
send "y\r"

expect "Reload privilege tables now? (Press y|Y for Yes, any other key for No) :"
send "y\r"

EOF

# Run Expect script.
# This runs the "mysql_secure_installation" script which removes insecure defaults.
sudo expect ~/secure_our_mysql.sh

# Cleanup
rm -v ~/secure_our_mysql.sh # Remove the generated Expect script

#sudo apt-get -qq purge expect > /dev/null # Uninstall Expect, commented out in case you need Expect

. ~/.bashrc # This reloads bashrc profile

echo "Don't forget to change root password."
