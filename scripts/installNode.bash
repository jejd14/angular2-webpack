#!/bin/bash
# Script is tested on Ubuntu 16.04
# Script needs to be executed with . ./installNode.bash

sudo apt-get update # Update system repositories

# Install dependencies that the npm script needs
sudo apt-get install -y build-essential
sudo apt-get install -y libssl-dev
sudo apt-get install -y curl

if [[ $(node --version) != "v6.9.5" ]];    then
    # From https://github.com/creationix/nvm#installation
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
    export NVM_DIR="$HOME/.nvm"

    [ -s "$NVM_DIR/nvm.sh" ]
    . "$NVM_DIR/nvm.sh" # This loads nvm

    nvm install 6.9.5 # Install node v6.9.5
    nvm use 6.9.5
    nvm alias default 6.9.5
    node -v

    npm install -g npm # Update to latest npm
    nvm --version
else
    echo "Node v6.9.5 is already installed."
fi

# From https://github.com/guard/listen/wiki/Increasing-the-amount-of-inotify-watchers
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

. ~/.bashrc # This reloads bashrc profile
