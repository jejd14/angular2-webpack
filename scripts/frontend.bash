#!/bin/bash

# go into the frontend folder
cd frontend

# install dependencies and run tests
npm install
npm run postinstall
npm test

# go out of the frontend folder
cd ..
